= SRC FAQ =
version 1.6

[[why]]
== Why SRC instead of $VCS? ==

Most version control systems today are multi-user, multi-file, and
multi-branch oriented. These are all good features and properties to
have, but they neglect the need to maintain simple single-file
documents, such as HOWTOs and FAQs, much like the very file you are
reading now. There is even a good use-case for small programs and
scripts. Do you presently keep your `~/bin` contents under version
control? If not, consider using SRC for them.

[[another-vcs]]
== $VCS already does single-file version control, why another one? ==

It is true, other VCSes already fulfill this simple criterion, SCCS
and RCS being some of the earliest examples dating back to the 1970s
and 1980s. While SCCS died off due to its proprietary nature,
https://www.gnu.org/software/rcs[RCS] has kept a niche for itself
precisely for single-file projects. In fact, SRC is built on top of
RCS, rather than reimplementing all of the gritty master file details.

The idea that spawned the development of SRC was that it would
have these five properties:

1. Only deals with single files. Use for HOWTOs, memoranda, scripts, etc.

2. Allows multiple histories to live in the same directory without entanglement.

3. Has a human-readable master-file representation - no binary blobs.

4. Modern CLI user-interface. Commands familiar to Subversion, Hg, Git users.

5. Integer sequential revision numbers a la Subversion.

Notably, RCS itself fails on the latter two criteria. Designed both as
an early attempt at VCS and for multi-user environments, the commands
are awkward to deal with and it requires complicated processes of
locking and unlocking files in order to edit and commit them. None of
this is appropriate anymore. Modern DVCSes with a non-locking model
have proven more effective even for multi-user projects, let alone
single-user ones that will never have contention issues.

Other projects to mold Mercurial and Git for a single-file purpose
will at the _very_ least fail criteria #3 and #5, and often #4 as
well.

[[dvcs-obsoletion]]
== Does SRC mean that $DVCS is obsolete? ==

Absolutely not! SRC and DVCSes serve entirely opposite needs. SRC’s
strength is precisely when there is neither need nor desire for
collaboration or publishing features, when there is only a single file
and a single author for a file. In fact, if your script grows into a
full project in its own right, SRC has a `src fast-export` command that
can be used to jump-start a DVCS repository with the entire existing
history intact.

SRC might make certain uses of DVCS obsolete, such as keeping
individual documents tucked away in their own directories so that the
DVCS (which usually has a special repository directory named like
`.hg` or `.git`) can operate. Scripts to impose a single-file concept
on top of these systems do not go <<another-vcs,far enough>> with
respect to the reasons SRC exists.

[[self-hosting]]
== Is SRC used to develop itself? ==

No.  That would be silly.  SRC is a real software project, with
several files that do, and should, have joint history.  It has 
a public Git repository.

SRC has real users (that is, other than the author) since day
two after it was announced, but not for projects like itself.

[[bare-rcs]]
== I still use plain RCS for single files. Why move to SRC? ==

Because process friction matters, even to hackers as ancient as 
you and the author of SRC. Bare RCS's interface is pretty awful;
a competently-designed UI can reduce that friction considerably.
Try it and see.

If you use RCS through Emacs's VC mode, you already have a pretty
good wrapper around it...which, as it happens, the author of SRC
also wrote.  SRC has a couple of advantages over VC-RCS - running
lockless, better scriptability, and ditching RCS's ugly revision
numbering are the main ones.

SRC is now a fully-supported VC-mode system in the Emacs development
tree, so you can run SRC through Emacs and get the lockless operation
and Subversion-style revision numbers.

[[beginners-vcs]]
== Is SRC a good system on which to learn about version control? ==

*YES*! SRC is explicitly designed to have the bare-bones features and
commands of a modern version control system. Keep in mind that SRC’s
strength is single-file documents and projects. If you have loose
scripts and documents not presently under any version control, SRC is
a good candidate for playing around with them.

If instead you have a large multi-file project, ease yourself into
using a DVCS with simple commands, possibly even using SRC’s command
set as a guideline for which ones to learn first. You will appreciate
having actual changesets that span multiple files in this use
case. http://mercurial.selenic.com/[Mercurial] and
http://git-scm.com/[Git] are the most common, which means they are
also easy to find help for.

[[keywords]]
== Does SRC have keyword expansion? ==

No. When SRC commits a file with RCS on the backend, it uses `-kb` which
explicitly disables all kind of expansion, and also allows arbitrary
binary files to be stored.  The SCCS backend always checks out files
with -e, implying -k and no keyword expansion.

Keyword expansion has, in general, not been well-accepted in the VCS
world, most modern VCSes do not support it at all, and the author of
SRC thinks it was a terrible idea. Do not even suggest this feature,
it will not be implemented.

[[missing-features]]
== Does SRC have $FEATURE? ==

If you don’t see it in the `src help` listing, probably not. You are
certainly free to suggest features, but SRC is developed with extreme
conservatism as to what features to implement or not. Remember,
single-file, single-user, private VCS.

Before requesting a feature, ask yourself whether it makes SRC more
complicated, whether it really helps a single author or developer, and
whether it really makes sense to deploy SRC for your use-case instead of
a DVCS. These can all be hard questions, and if you are in doubt, you
may go forth with your request, others may share their own opinions.

SRC shines in its simplicity. Any individual feature is not
necessarily against this, but too many can easily creep over into
``too complicated'' territory.

[[network]]
== How well does SRC handle files over the network? ==

The answer is either ``completely fine'' or ``not at all'', depending on
what is being asked. :-)

SRC makes no special provisions, it operates in the current working
directory whether that is local storage, NFS, CIFS, sshfs, or any
other kind of networking file system. As long as the directory tree is
mounted on your system, SRC should be able to handle it.

[[status-inconsistency]]
== Why doesn’t src status display show the same letters as $VCS? ==

Ah, yes, everybody's favorite bikeshedding topic.

Consistency with other version control systems is an important way to
reduce any kind of surprises while using SRC. Unfortunately, the
single-letter codes used for statuses are not identical between VCSes
and often conflict with each other over specific meanings. For
example, *D* means 'deleted' in Subversion and Git, but Mercurial uses
*R* for that same meaning. Git uses *R* to mean 'renamed', while
Subversion uses it to mean 'replaced'.

It is an unfortunate state of affairs. The development philosophy
behind SRC is to keep it as un-innovative and unsurprising as
possible, but since multiple VCSes in widespread use have not
converged on the same meanings for single-letter status codes, SRC
needs to settle on its own definitions that may differ from what you
are used to.

[[SCCS]]
== Why did you bother with SCCS support? ==

Because some hacks that are useless or marginal are way too funny not to
do anyway.  This one was *hilarious*...if you have a hacker's sense of
humor. If you don't, never mind.

[[not-a-joke]]
== Um.  This is all a joke, right? ==

No, though the author admits he did laugh a lot while roughing out the
original design. Resurrect RCS?  Wrap it in a decent UI?  _Really?_

There's a significant amount of ha-ha-only-serious here.  Laugh, but
treat SRC as a lesson in several things.  Unix minimalism.  The virtue
of re-use, even of technology as old as RCS.  The effectiveness of a
carefully crafted UI.  The value of a conservative design with no
surprises and no undue cleverness.

[[reporting-bugs]]
== How should I report bugs in SRC? ==

This isn't really a FAQ, as nobody has asked it frequently enough; SRC
is simple, carefully-tested code and bugs in it have been
rare. Still...

The first thing to do is pull the tip version from the project
repository and verify that it still exhibits the bug.  If it
doesn't, you need read no further.

When you a bug persists with the repository tip version, the author
needs three things:

1. An exact description of how you tickled the bug, which should at
least include the SRC command or sequence of commands that produced
it.

2. A copy of the repository file(s) over which the bug was triggered.

3. The output of the following commands:

-------------------------------
uname -a
src version
python --version
rcs --version
sccs --version
-------------------------------

Points for style if you send a tarball that unpacks into a
directory containing relevant workfiles, their .src/RCS/SCCS
subdirectory, a file of SRC commands to reproduce the bug (named
'tickleme' or 'fooup' or something distinctive), and a README listing
your version info and circumstances.

If you can supply all this information, expect bug resolution to
be very rapid.

[[ack]]
== Acknowledgments ==

Most of this FAQ was originally written by Mike Swanson, aka chungy.
